# Build Instructions

```bash
git clone git@gitlab.com:sphewitt/openfoam-scripts.git

# Build OpenFOAM and ThirdParty
git submodule init
git submodule update
```
