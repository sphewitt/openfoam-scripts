#!/usr/bin/env bash

# Set to use system mpi
sed -e "s/SYSTEMOPENMPI/SYSTEMMPI/" -i OpenFOAM-8/etc/bashrc

# Add the mpi root
sed '/# Set environment variables/a export MPI_ROOT=${MPICH_DIR}' OpenFOAM-8/etc/config.sh/settings

# Fix the compilers
sed -e "s/gcc/cc/" -i OpenFOAM-8/wmake/rules/linux64Gcc/c
sed -e "s/g++/CC/" -i OpenFOAM-8/wmake/rules/linux64Gcc/c++

# Overwrite current ThirdParty Makefile.inc
cp Makefile.inc ThirdParty-8/etc/wmakeFiles/scotch/Makefile.inc.i686_pc_linux2.shlib-OpenFOAM

# Build OpenFOAM
sbatch openfoam-build.jcf
