#!/usr/bin/env bash

# Set Base/Clean directories
BASE=${PWD}
BATCHDIR=sbatch

# Create working directory
mkdir -p ${BATCHDIR}

# Set Tasks/Node
ppn=36

for node in 1 4 8; do

  JOB=nodes-${node}
 
  cp -r jcf.orig ${BATCHDIR}/${JOB}

  sed -e "s/NODES/${node}/" -e "s/PPN/${ppn}/" -i ${BATCHDIR}/${JOB}

done
