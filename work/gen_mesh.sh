#!/usr/bin/env bash

# Load OpenFOAM module and source etc/bashrc
## module load openfoam/8
## source <path>/etc/bashrc

# Set Base/Clean directories
BASE=${PWD}
CLEAN=${BASE}/clean/pitzDaily
TMP=${BASE}/tmp-case-dir
RUN=${BASE}/run

# Copy clean to tmp-dir
mkdir -p ${TMP}
cp -r ${CLEAN}/* ${TMP}

# Build initial mesh for case
cd ${TMP}
blockMesh 
refineMesh -overwrite

# Create working directory
mkdir -p ${RUN}

for node in 1 4 8; do

  cores=$(( ${node} * 36 ))

  # Update mesh size in case-dir
  refineMesh -case ${TMP} -overwrite

  # Create a new node dir
  CASE=${RUN}/nodes-${node}
  mkdir -p ${CASE}

  cp -r ${TMP}/* ${CASE}/.

  # Decompose Case
  sed -e "s/CORES/${cores}/" -i ${CASE}/system/decomposeParDict 
  decomposePar -case ${CASE}

done


rm -rf ${TMP}
